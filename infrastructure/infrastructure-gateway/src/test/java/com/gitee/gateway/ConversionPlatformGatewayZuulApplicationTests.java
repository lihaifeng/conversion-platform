/*
 * Copyright (c) 2019-2019
 */

package com.gitee.gateway;

import org.junit.Test;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
class GatewayZuulApplicationTests {

  @Test
  void contextLoads() {
  }
}
