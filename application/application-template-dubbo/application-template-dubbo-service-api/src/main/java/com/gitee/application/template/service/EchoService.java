package com.gitee.application.template.service;

/**
 * @author lihaifeng
 */
public interface EchoService {

  /**
   * ping pong
   * @param message
   * @return
   */
  String echo(String message);
}