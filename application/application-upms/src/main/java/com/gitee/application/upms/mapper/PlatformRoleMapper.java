/*
 * Copyright [2020] [lihaifeng,xuhang]
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package com.gitee.application.upms.mapper;

import java.util.List;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.gitee.common.upms.dao.PlatformRoleDO;

/**
 * <p>
 * 角色表 Mapper 接口
 * </p>
 *
 * @author lihaifeng
 * @since 2020-01-12
 */
public interface PlatformRoleMapper extends BaseMapper<PlatformRoleDO> {

  /**
   * 通过id查找用户具体角色信息
   * @param id
   * @return 用户角色集合
   */
  List<PlatformRoleDO> listRolesByUserId(Long id);
}
