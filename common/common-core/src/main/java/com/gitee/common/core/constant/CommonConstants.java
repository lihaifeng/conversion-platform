/*
 *    Copyright [2020] [lihaifeng,xuhang]
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.gitee.common.core.constant;

/**
 * @author lihaifeng
 * @date 2017/10/29
 */
public interface CommonConstants {
  /**
   * header 中租户ID
   */
  String TENANT_ID = "TENANT-ID";

  /**
   * header 中版本信息
   */
  String VERSION = "VERSION";

  /**
   * 租户ID
   */
  Integer TENANT_ID_1 = 1;

  /**
   * 删除
   */
  String STATUS_DEL = "1";
  /**
   * 正常
   */
  String STATUS_NORMAL = "0";

  /**
   * 锁定
   */
  String STATUS_LOCK = "9";

  /**
   * 菜单
   */
  String MENU = "0";

  /**
   * 菜单树根节点
   */
  Integer MENU_TREE_ROOT_ID = -1;

  /**
   * 编码
   */
  String UTF8 = "UTF-8";

  /**
   * 前端工程名
   */
  String FRONT_END_PROJECT = "pigx-ui";

  /**
   * 后端工程名
   */
  String BACK_END_PROJECT = "pigx";

  /**
   * 公共参数
   */
  String PIG_PUBLIC_PARAM_KEY = "PIG_PUBLIC_PARAM_KEY";

  /**
   * 成功标记
   */
  Integer SUCCESS = 0;
  /**
   * 失败标记
   */
  Integer FAIL = 1;

  /**
   * 默认存储bucket
   */
  String BUCKET_NAME = "lihaifeng";
}
