package com.gitee.common.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.gitee.common.model.OperateLog;
import org.apache.ibatis.annotations.Mapper;

/**
 * @author xuhang
 */
public interface OperateLogMapper extends BaseMapper<OperateLog> {

}
