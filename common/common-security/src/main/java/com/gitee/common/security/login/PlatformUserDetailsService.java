package com.gitee.common.security.login;

import org.springframework.security.core.userdetails.UserDetailsService;

/**
 * @author lihaifeng
 */
public interface PlatformUserDetailsService extends UserDetailsService {

}
